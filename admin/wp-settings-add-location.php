<?php
wp_enqueue_style('wpstyle');
?>
<div class="wrap">
    <?php
    global $wpdb;
    $description="";
    $latitude="";
    $longitude="";
    $table_name = $wpdb->prefix . "marker";
    
    if (isset($_POST["latitude"]) && isset($_POST["longitude"])) {
    ?>
    <div class="updated"><p>
    <?php
        $description = sanitize_text_field($_POST["description"]);
        $latitude = sanitize_text_field($_POST["latitude"]);
        $longitude = sanitize_text_field($_POST["longitude"]);

        $wpdb->insert (
            $table_name,
            array (
                'description' => $description,
                'latitude' => $latitude,
                'longitude' => $longitude
            )
        );

        _e('Location saved.', PLUGIN_NAME)
    ?>
    <p></div>
    <?php } ?>
    <h2><?php _e('Add a New Location', PLUGIN_NAME);?></h2>
    <form method="post" action="">
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row">
                        <label for="description"><?php _e('Description', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="description" name="description" size="50" maxlength="50"
                               value="<?php print($description);?>">                     
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="latitude"><?php _e('Latitude', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="latitude" name="latitude" size="9" maxlength="9"
                               value="<?php print($latitude);?>" required>                     
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="longitude"><?php _e('Longitude', PLUGIN_NAME);?>:</label>
                    </th>
                    <td>
                        <input id="longitude" name="longitude" size="9" maxlength="9"
                               value="<?php print($longitude);?>" required>                     
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="submit" class="button button-primary" value="<?php _e('Save', PLUGIN_NAME) ?>">
    </form>
</div>