<?php
class Wp_gmaps_class {
    
        public function __construct() {
            load_plugin_textdomain(PLUGIN_NAME, false, basename( dirname( __FILE__ ) ) . '/languages');
            
            add_action ( 'init', array($this, 'init') );
            add_action ( 'widgets_init', array($this, 'register_widget') );
            add_action ( 'admin_menu', array($this, 'setup_admin_menu') );
        }
        
        public function init() {
            wp_register_style('wpstyle', plugins_url('css/style.css', __FILE__));
            wp_register_script('wpgmaps_sensor', '//maps.google.com/maps/api/js?sensor=true', array('jquery'), false, false);
            wp_register_script('wpgmaps', plugins_url('js/gmaps.js', __FILE__), array('jquery'), false, false);  
            add_shortcode('Map', array($this, 'map_shortcode'));
            
            add_action( 'wp_enqueue_scripts', 'shortcode_enqueue');
        }
        
        public function setup_admin_menu() {
            add_object_page(PLUGIN_NAME, __('GMaps', PLUGIN_NAME), 'manage_options',
                    PLUGIN_NAME, array($this, 'admin_map_page'));
            add_submenu_page(PLUGIN_NAME, __('Map', PLUGIN_NAME), __('Map',
                    PLUGIN_NAME), 'manage_options', PLUGIN_NAME, array($this, 'admin_map_page'));
            add_submenu_page(PLUGIN_NAME, __('Locations', PLUGIN_NAME), __('Locations',
                    PLUGIN_NAME), 'manage_options', 'locations', array($this, 'admin_locations_page'));
            add_submenu_page(PLUGIN_NAME, __('Add Location', PLUGIN_NAME), __('Add Location',
                    PLUGIN_NAME), 'manage_options', 'add-location', array($this, 'admin_add_location_page'));
        }
        
        public function map_shortcode() {
            include_once(plugin_dir_path(__FILE__) . 'wp-gmaps-map.php');
        }
        
        public function register_widget() {
            register_widget('Wp_gmaps_widget');
        }
        
        public function admin_map_page() {
            include_once(plugin_dir_path(__FILE__) . 'admin/wp-settings-map.php');
        }
        
        public function admin_locations_page() {
            include_once(plugin_dir_path(__FILE__) . 'admin/wp-settings-locations.php');
        }
        
        public function admin_add_location_page() {
            include_once(plugin_dir_path(__FILE__) . 'admin/wp-settings-add-location.php');
        }
}

add_action( 'plugins_loaded', 'wp_gmaps_init');

function wp_gmaps_init() {
    $wp_gmaps = new Wp_gmaps_class();
}

function shortcode_enqueue() {
    global $post;
    
    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'Map') ) {
        wp_enqueue_script('wpgmaps_sensor');
        wp_enqueue_script('wpgmaps');
    }
}