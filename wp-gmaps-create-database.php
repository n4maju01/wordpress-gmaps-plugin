<?php
class Wp_gmaps_create_database {
    public static function activate() {
        global $wpdb;
        $table_name1 = $wpdb->prefix . "map";
        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name1 (
                id int PRIMARY KEY AUTO_INCREMENT,
                latitude varchar(9) NOT NULL,
                longitude varchar(9) NOT NULL,
                address varchar(50),
                zip varchar(15),
                city varchar(50)
            );";
        
        require_once(ABSPATH . "wp-admin/includes/upgrade.php");
        dbDelta($sql);
        
        $wpdb->insert (
            $table_name1,
            array (
                'latitude'  => 65.00643,
                'longitude' => 25.49078,
                'address'   => "Teuvo Pakkalan katu 19",
                'zip'       => "90130",
                'city'      => "Oulu"
            )
        );
        
        $table_name2 = $wpdb->prefix . "marker";
        
        $sql = "CREATE TABLE IF NOT EXISTS $table_name2 (
                id int PRIMARY KEY AUTO_INCREMENT,
                description varchar(50),
                latitude varchar(9) NOT NULL,
                longitude varchar(9) NOT NULL
            );";
        
        require_once(ABSPATH . "wp-admin/includes/upgrade.php");
        dbDelta($sql);
        
        $wpdb->insert (
            $table_name2,
            array (
                'description' => 'Teuvo Pakkalan kampus',
                'latitude'    => 65.00643,
                'longitude'   => 25.49078
            )
        );
        
        add_option("wp-gmaps-db-version", "0.1");
    }
    
    public static function uninstall() {
        global $wpdb;
        $table_name = $wpdb->prefix . "map";
        $sql = "DROP TABLE IF EXISTS $table_name;";
        $wpdb->query($sql);
        $table_name = $wpdb->prefix . "marker";
        $sql = "DROP TABLE IF EXISTS $table_name;";
        $wpdb->query($sql);
        delete_option("wp-gmaps-db-version");
    }
}