<?php
/* 
Plugin Name: WP-GMaps
Plugin URI: http://www.foo.com
Description: Gmaps.js-kirjastoa hyödyntävä Google Maps-karttalisäosa.
Version: 0.1
Author: Jussi Mannermaa
Author URI: http://n4maju01.studyingroom.net
License: GPL2
*/
?>
<?php
define('PLUGIN_NAME', 'wp-gmaps');

include_once('wp-gmaps-widget.php');
include_once('wp-gmaps-class.php');
include_once('wp-gmaps-create-database.php');

//Add hooks for activation and uninstallation. Implementation is on a separate static class.
register_activation_hook(__FILE__, array('Wp_gmaps_create_database', 'activate'));
register_uninstall_hook(__FILE__, array('Wp_gmaps_create_database', 'uninstall'));